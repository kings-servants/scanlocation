
#ifndef LESSON2_SCAN_TO_ContnavLogger_H
#define LESSON2_SCAN_TO_ContnavLogger_H
//#pragma once

#include <time.h>
#include <stdarg.h>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <string>
#include <stdexcept>
#include <fstream>
#include <mutex>
#include <iostream>
using std::string;
namespace contnav {
    namespace     log {
#define contnav_debug(format, ...) \
      ContnavLogger::instance()->log(ContnavLogger::DEBUG, __FILE__, __LINE__, format, ##__VA_ARGS__)

#define contnav_info(format, ...) \
      ContnavLogger::instance()->log(ContnavLogger::INFO, __FILE__, __LINE__, format, ##__VA_ARGS__)

#define contnav_warn(format, ...) \
      ContnavLogger::instance()->log(ContnavLogger::WARN, __FILE__, __LINE__, format, ##__VA_ARGS__)

#define contnav_error(format, ...) \
      ContnavLogger::instance()->log(ContnavLogger::ERROR, __FILE__, __LINE__, format, ##__VA_ARGS__)

#define contnav_fatal(format, ...) \
      ContnavLogger::instance()->log(ContnavLogger::FATAL, __FILE__, __LINE__, format, ##__VA_ARGS__)

        class ContnavLogger
        {
        public:
            enum Level
            {
                DEBUG = 0,
                INFO,
                WARN,
                ERROR,
                FATAL,
                LEVEL_COUNT
            };

            ContnavLogger();
            ~ContnavLogger();

            static ContnavLogger* instance();
            void open(const string &logfile);
            void close();
            void max(int bytes);
            void level(int level);

        public:
            void log(Level level, const char* file, int line, const char* format, ...);

        protected:
            // FILE *m_fp;
            static const char* s_level[LEVEL_COUNT];
            static ContnavLogger *m_instance;
            void rotate();
        private:
            string m_filename;
            std::ofstream m_fout;
            int m_max; //日志最大的长度，超过就另外存一个新的文件
            int m_len;//当前的日志有多长
            int m_level;
            std::mutex m_ ;
        };

    }}

#endif // LESSON2_SCAN_TO_ContnavLogger_H